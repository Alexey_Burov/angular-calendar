import {Component, OnInit} from '@angular/core';
import {DbService} from './services/db.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DbService]

})
export class AppComponent implements OnInit {
  title = 'angular-calendar';

  db: any;
  constructor(private dbService: DbService){}

  ngOnInit() {

  }
}
