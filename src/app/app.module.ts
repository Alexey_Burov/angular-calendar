import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import { FooterComponent } from './footer/footer.component';
import {
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatDatepickerModule,
  MAT_DATE_LOCALE,
  MatNativeDateModule, MatGridListModule, MatOptionModule, MatSelectModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {LayoutModule} from '@angular/cdk/layout';
import { CasseDialogComponent } from './constructionsite/casse-dialog/casse-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ConstructionsiteComponent} from './constructionsite/constructionsite.component';
import {HeaderComponent} from './header/header.component';
import {DaysCardComponent} from './days-card/days-card.component';
import {MomentModule} from 'angular2-moment';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    CasseDialogComponent,
    ConstructionsiteComponent,
    HeaderComponent,
    DaysCardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
    AppRoutingModule,
    MatCardModule,
    LayoutModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatOptionModule,
    MatSelectModule,
    MomentModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
  ],
  exports: [
    CasseDialogComponent,
  ],
  entryComponents: [
    CasseDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
