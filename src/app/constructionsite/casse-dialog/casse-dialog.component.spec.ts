import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasseDialogComponent } from './casse-dialog.component';

describe('CasseDialogComponent', () => {
  let component: CasseDialogComponent;
  let fixture: ComponentFixture<CasseDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasseDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
