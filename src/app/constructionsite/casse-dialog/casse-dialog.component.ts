import {Component, ElementRef, Inject, LOCALE_ID, OnInit, Renderer2, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Day} from './day';
import {MatDatepickerInputEvent} from '@angular/material';
import {CaseService} from '../../services/case.service';
import * as moment from 'moment';
import {NameList} from '../../services/name-list';
import {DbService} from '../../services/db.service';


class DialogData {
}

@Component({
  selector: 'app-casse-dialog',
  templateUrl: './casse-dialog.component.html',
  styleUrls: ['./casse-dialog.component.css'],
})
export class CasseDialogComponent implements OnInit {

  reactiveForm: FormGroup;

  nameList = new NameList();
  day = new Day();

  @ViewChild('select', {static: false})
  private select: ElementRef;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
              private fb: FormBuilder,
              private renderer: Renderer2,
              @Inject(CaseService) private caseService: CaseService,
              private dbservice: DbService
  ) {
  }

  ngOnInit() {

    this.initForm();
  }

  // Инициализация формы
  initForm() {
    this.reactiveForm = this.fb.group({
      task: ['Задачи'],
      description: ['description'],
      case: {text: 'Праздник', icon: 'cake'},
      date: ['date'],
      time: ['8']
    });

  }


  buildListHours(type: string, event: MatDatepickerInputEvent<Date>) {


    const d = this.reactiveForm.value.date;
  }

  saveFormForCase() {
    const controls = this.reactiveForm.controls;

    /// Проверяем форму на валидность
    if (this.reactiveForm.invalid) {
      /// Если форма не валидна, то помечаем все контролы как touched
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());

      return;

    }

    const form = this.reactiveForm.value;
    const date = moment(form.date);

    this.caseService.addCase(date.toDate().valueOf(), form.time, form.case['text'], form.case['icon'], form.task, form.description);
    this.dbservice.insert([date.toDate().valueOf() + form.time, form.case['text'], form.case['icon'], form.description]);
  }

}
