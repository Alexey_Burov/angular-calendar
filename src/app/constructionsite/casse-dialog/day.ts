
export class Hour {
  disabled?: boolean;
  name: string;
  value: number;
  case: Case;
}

export class Case {
  name: string;
  head: string;
  description: string;
  color: string;
}

export class Day {

  caseList: Array<Hour> = [];

  constructor() {
    for (let i = 8; i < 22; i++) {
      this.caseList.push({disabled: false, name: `${i} часов`, value: 8, case: null});
    }
  }


}
