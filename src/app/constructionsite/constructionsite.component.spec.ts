import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionsiteComponent } from './constructionsite.component';

describe('ConstructionsiteComponent', () => {
  let component: ConstructionsiteComponent;
  let fixture: ComponentFixture<ConstructionsiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructionsiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
