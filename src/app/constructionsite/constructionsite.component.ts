import {Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {MatDialog} from '@angular/material';
import {CasseDialogComponent} from './casse-dialog/casse-dialog.component';
import {DateService} from '../services/date.service';
import {CaseService} from '../services/case.service';
import {Day} from './casse-dialog/day';
import {DaysCardComponent} from '../days-card/days-card.component';


@Component({
  selector: 'app-month',
  templateUrl: './constructionsite.component.html',
  styleUrls: ['./constructionsite.component.css'],
})
export class ConstructionsiteComponent implements OnInit {

  type: string;
  dayName: string;
  currentDay: Day;

  readonly COUNTGRID = 42;

  @ViewChildren(DaysCardComponent) private counterComponentDay: QueryList<DaysCardComponent>;

  openDialog(e) {
    this.dialog.open(CasseDialogComponent, {
      data: {
        date: '0:0 1.01'
      }
    });
  }

  goToMonth(e) {
    if (e) {
      this.dateService.anotherMonth('next');
    } else {
      this.dateService.anotherMonth('previous');
    }


    this.counterComponentDay.forEach((item, index) => {
      item.firstDay =  this.dateService.firstDay();
      item.initCard(this.dateService.currentMonthNum());
    });
  }
  range(num: number) {
    return [...Array(num).keys()];
  }

  constructor(
    public dialog: MatDialog,
    private dateService: DateService,
    private caseService: CaseService
  ) {

  }

  ngOnInit(): void {
    this.dateService.init(new Date());
    this.type = 'month';
  }

  setDay(date: Date) {
    this.currentDay = this.caseService.getCaseInDay(date.valueOf());
    this.type = 'day';
    this.dayName = this.dateService.nameOfDay(date.getDay());
  }

  search(){

  }

  backward(e) {
    this.type = 'month';
  }
}
