import {Component, EventEmitter, Input, OnInit, OnDestroy, Output} from '@angular/core';
import {CaseService} from '../services/case.service';
import {Case, Day} from '../constructionsite/casse-dialog/day';
import {Observable, Subscription} from 'rxjs';
import {min} from 'rxjs/operators';
import {DateService} from '../services/date.service';


@Component({
  selector: 'app-days-card',
  templateUrl: './days-card.component.html',
  styleUrls: ['./days-card.component.css']
})

export class DaysCardComponent implements OnInit, OnDestroy {

 constructor(
   private caseService: CaseService,
   private dateService: DateService
 ) { }

  @Input() name = '';
  @Input() day: number;
  @Input() month: number;
  @Input() firstDay: number;
  @Output() gotoDay = new EventEmitter();

  buttonList: string[];
  cases: Case[];
  date: Date;
  num: number;

  private subscriptions: Subscription[] = [];

  initCard(month: number) {
    this.date = new Date(this.dateService.date().getFullYear(), month, this.day - this.firstDay + 1);
    this.num = this.date.getDate();
    this.buttonList = [];
    const caseSub = this.caseService.list$
      .subscribe(() => {
        this.addCase();
      });
    this.subscriptions.push(caseSub);
    this.cases = this.caseService.getCaseInDay(this.date.valueOf());
  }

  ngOnInit() {

    this.initCard(this.dateService.currentMonthNum());
  }

  ngOnDestroy() {
    this.subscriptions
      .forEach(s => s.unsubscribe());
  }

  addCase() {
    this.buttonList = [];
    const day: Day = this.caseService.list$.getValue()[`${this.date.valueOf()}`];
    if (day) {
      for (let i = 0; i < day.caseList.length; ++i) {
        if (day.caseList[i].disabled) {
          this.buttonList.push(day.caseList[i].case.name);
        }
      }
    }
  }

  containsCase(): boolean{
    return true;
  }
  cassesClick(event) {
    this.gotoDay.emit(this.date);
    // this.openDialog();
  }

  rand(val: number): number {
    return Math.round(Math.random() * val);
  }


}
