import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() {
  }

  mHeader: string;
  type: string;

  @Input('type')
  set etype(value){
    this.type = value;
  }
  @Input('header')
  set еheader(value) {
    // 'Текущий месяц или день';
    this.mHeader = value;
  }

  @Output() setNewMonth = new EventEmitter();
  @Output() setBackward = new EventEmitter();
  ngOnInit() {
    this.type = 'month';
  }

  @Output() setNewCasse = new EventEmitter();


  goNext() {
    this.setNewMonth.emit(true);
  }

  goPrev() {
    this.setNewMonth.emit(false);
  }

  backward() {
    this.setBackward.emit();
  }

  addCasse() {
    this.setNewCasse.emit();
  }

  search(str){
    console.log(str);
  }
}
