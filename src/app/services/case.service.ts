import { Injectable } from '@angular/core';
import {Hour, Day, Case} from '../constructionsite/casse-dialog/day';
import {BehaviorSubject} from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class CaseService {

  allCaseList = {};

  constructor() { }


  list$: BehaviorSubject<{}> = new BehaviorSubject<{}>({});
  setCase(list: {}) {
    this.list$.next(this.allCaseList); // тут мы поставим
  }

  addCase(d: number, time: number, caseName: string, caseIcon: string, tagOfDay: string, descriptionString: string) {

    let c: Case;
    let day: Day;
    let h: Hour;

    const date = new Date(d);

    if (!this.allCaseList[d]) {

      day = new Day();
      this.allCaseList[d] = day;
    }
    day = this.allCaseList[d];

    h = day.caseList[time - 8];

    if (!h.case) {
      h.case = new Case();
    }
    c = h.case;

    c.name = caseIcon;
    c.description = descriptionString;

    c.head = caseName;

    h.disabled = true;
    h.name = time + ' часов';
    h.value = time;
    h.case = c;

    day.caseList[time - 8] = h;
    this.allCaseList[d] = day;
    this.setCase(this.allCaseList[d]);
  }


  getCaseInDay(date: number) {
    // const cases = Case[];

    if (this.allCaseList[date]) {

      return this.allCaseList[date];
    } else {
      return [];
    }
  }

}
