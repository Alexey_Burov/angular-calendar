import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {NameList} from './name-list';

@Injectable({
  providedIn: 'root'
})
export class DateService {


private currentMoment: moment.Moment;
  private _nameList: NameList;

  constructor() {
    this.init();
    this._nameList = new NameList();
  }

  private _start: Date;

  get start(): Date {
    return this._start;
  }

  set start(value: Date) {
    this._start = value;
  }

  init(date?: Date) {
    if (date) {
      this.currentMoment = moment(date);
    } else {
      this.currentMoment = moment(new Date());
    }
  }

  currentMonthNum() {
    return this.currentMoment.month();
  }

  firstDay(montNum?: number) {
    const m = moment(
      new Date(this.currentMoment.year(),
        montNum ? montNum : this.currentMoment.month(),
        1));

    return m.day() ? m.day() - 1 : m.day() + 6;
  }

    date() {
    return this.currentMoment.toDate();
  }
  daysInMonth() {
    return this.currentMoment.daysInMonth();
  }

  anotherMonth(step: ('next' | 'previous')) {
    step === 'next' ?
      this.currentMoment.month(this.currentMoment.month() + 1) :
      this.currentMoment.month(this.currentMoment.month() - 1);
    return this.currentMoment.month();
  }

  nextDay() {
    this.setCurrentDay(this.currentMoment.day() + 1);
    return this.currentMoment.day();
  }

  prevDay() {
    this.setCurrentDay(this.currentMoment.day() - 1);
    return this.currentMoment.day();
  }

  nameOfMonth(num?: number) {
    if (!num) {
      return this._nameList.monthNameList[this.currentMoment.month()];
    } else {
      return this._nameList.monthNameList[num];
    }
  }

  nameOfDay(num: number) {
    return this._nameList.dayNameList[num];
  }

  setCurrentDay(num: number) {
    this.currentMoment.day(num);
    return this.date();
  }

  toDateOfDay(year: number, numMonth: number, numDay: number) {
    return new Date(year, numMonth, numDay);
  }
}
