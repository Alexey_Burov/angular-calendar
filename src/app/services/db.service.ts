import { Injectable } from '@angular/core';
import '../../scripts/sql.js';

@Injectable({
  providedIn: 'root'
})
export class DbService {


  private static DB_NAME = 'calendar';
  private static  INSTANCE = false;

  constructor() {
    if (! DbService.INSTANCE) {
      DbService.INSTANCE = true;
      init(DbService.DB_NAME);
    }
  }


  static init(dbName: string) {

  }

  select(data: string) {
    select(data);
  }

  insert(data) {
    insert(data);
  }

  update(data: string) {
    update(data);
  }

  del(data: string) {
    del (data);
  }

}
