declare function init(dbName: string): void;
declare function select(data: string): void;
declare function insert(data: string): void;
declare function update(data: string): void;
declare function del(data: string): void;
declare function testDB();
