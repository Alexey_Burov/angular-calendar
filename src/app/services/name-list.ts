export class NameList {
  monthNameList = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];
  dayNameList = [
    'понедельник',
    'вторник',
    'среда',
    'четверг',
    'пятница',
    'суббота',
    'воскресенье'
  ];
  tagsList = [
    '',
    'Задачи.',
    'Отчеты.',
    'Планирование.',
    'Разработка.',
    'Дизайн.',
    'Проектирование.',
    'Технологии.',
    'Управление.',
    'Инфрмация.',
    'Потоки.',
    'Маркетинг.',
  ];
  cassesNameList = [
    { icon: 'eco', text: 'Эко технологии' },
    { icon: 'cake', text: 'Праздник' },
    { icon: 'directions_run', text: 'Срочное' },
    { icon: 'airplanemode_active', text: 'Выезд' },
    { icon: 'restaurant', text: 'Ресторан' },
    { icon: 'business_center', text: 'Деловая встреча' },
    { icon: 'emoji_food_beverage', text: 'Чай' },
    { icon: 'ac_unit', text: 'Заморожено' },
    { icon: '360', text: 'Обновление' },
    { icon: 'alarm', text: 'Напоминание' }
  ];

}
