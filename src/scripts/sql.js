;
{
  const requestCaseTable = 'CREATE TABLE cases(date INTEGER UNIQUE, name TEXT, icon TEXT, description TEXT);';

  let db = null;
  let name = null;

  function init(dbName) {
    db = openDatabase(dbName + '.db', '1.0', 'description', 1);
    create();
  }

  function select(table) {
    db.transaction(function (tx) {
      tx.executeSql(`SELECT * FROM ${table};`, [],
        function (tx, result) {

            console.log('select: ', result.rows);

        }, function (tx, error) {
          console.log(error);
        });
    });
  }

  function insert(data) {
    db.transaction(function (transaction) {
      transaction.executeSql(("INSERT INTO cases (date, name, icon, description) VALUES (?, ?, ?, ?);"),
        data, null,
        function (tx, error) {
          console.log(error.message);
        })
    });
  }

  function update(data) {

  }

  function del(data) {

  }

  function create() {
    db = openDatabase('calendar' + '.db', '1.0', 'description', 1);
    db.transaction(function (tx) {
      tx.executeSql('DROP TABLE cases', [], null, function (tx, error) {
        console.log(error.message);
      })
    });

    db.transaction(function (tx) {
      tx.executeSql(requestCaseTable,
        [], null, function (tx, error) {
          console.log(error.message);
        });
    });
  }
}
